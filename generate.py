# SPDX-FileCopyrightText: 2020-2024, Thomas Larsson
#
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import bpy
from bpy.props import *
from bpy_extras.io_utils import ExportHelper

#-------------------------------------------------------------
#   Operator Docs
#-------------------------------------------------------------

class OperatorDocGenerator:
    module : StringProperty(
        name = "Module",
        description = "Generate operator docs for this module",
        default = "daz"
    )

    def execute(self, context):
        if hasattr(bpy.ops, self.module):
            module = getattr(bpy.ops, self.module)
        else:
            return
        string = self.generate(module)
        filepath = self.getFilePath()
        with open(filepath, "w") as fp:
            fp.write(string)
        print("%s written" % filepath)
        return {'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}


    def getOpDescription(self, rnatype):
        if rnatype.description:
            return rnatype.description
        else:
            return "Undocumented operator"


    def getPropType(self, prop):
        if prop.type == "ENUM":
            items = prop.enum_items
            return ("enum in %s" % list(prop.enum_items.keys()))
        elif prop.subtype == "NONE":
            return prop.type.lower()
        else:
            return prop.subtype.lower()


    def getPropDescription(self, prop):
        if prop.description:
            return prop.description
        else:
            return prop.name


    def getPropDefault(self, prop):
        if prop.type == "STRING":
            return ("\"%s\"" % prop.default)
        elif prop.type == "INT":
            return ("%d" % prop.default)
        elif prop.type == "BOOLEAN":
            return ("%s" % prop.default)
        elif prop.type == "FLOAT":
            if prop.subtype == "NONE":
                return ("%.3f" % prop.default)
            elif prop.subtype == "COLOR":
                return ("(%.3f,%.3f,%.3f,%.3f)" % tuple(prop.default_array))
        elif prop.type == "ENUM":
            return ("'%s'" % prop.default)
        elif prop.type == "COLLECTION":
            return None
        else:
            print("Unknown prop type: %s" % prop.type)
            print(prop)
            halt

#-------------------------------------------------------------
#   HTML output
#-------------------------------------------------------------

class HtmlOutput:
    filepath : StringProperty(
        name="Output File",
        description="Output File",
        maxlen=1024,
        default="")
    filename_ext = ".html"
    filter_glob : StringProperty(default="*.html", options={'HIDDEN'})

    def getFilePath(self):
        return os.path.splitext(self.filepath)[0] + ".html"

#-------------------------------------------------------------
#   MD output
#-------------------------------------------------------------

class MdOutput:
    filepath : StringProperty(
        name="Output File",
        description="Output File",
        maxlen=1024,
        default="")
    filename_ext = ".md"
    filter_glob : StringProperty(default="*.md", options={'HIDDEN'})

    separator = "\n---\n"

    def getFilePath(self):
        return os.path.splitext(self.filepath)[0] + ".md"

    def formatHeader(self, hdr):
        words = hdr.split("(", 1)
        string = "**%s** " % words[0]
        if len(words[1]) > 3:
            string += "( *%s* )\n" % words[1].rstrip(")")
        else:
            string += "()\n"
        return string

#-------------------------------------------------------------
#   Text output
#-------------------------------------------------------------

class TextOutput:
    filepath : StringProperty(
        name="Output File",
        description="Output File",
        maxlen=1024,
        default="")
    filename_ext = ".txt"
    filter_glob : StringProperty(default="*.txt", options={'HIDDEN'})

    separator = "\n-----------------------------------------------------------------\n\n"

    def getFilePath(self):
        return os.path.splitext(self.filepath)[0] + ".txt"

#-------------------------------------------------------------
#   HTML operators
#-------------------------------------------------------------

"""
<span style="font-family: Verdana, sans-serif;">
<b>bpy.ops.daz.activate_all</b>(
<i><span style="color: blue;">prefix = "", type = ""</span></i>)</span><br />
<br />&nbsp; Select all morphs of this type<br /><br />
<b>Parameters:</b><br />
<ul>
<li>&nbsp; <span style="font-family: &quot;Trebuchet MS&quot;, sans-serif;">prefix</span> (<i>string</i>) - prefix</li>
<li>&nbsp; <span style="font-family: &quot;Trebuchet MS&quot;, sans-serif;">type</span> (<i>string</i>) - type</li>
</ul>
<br />
"""

class DOCS_OT_OperatorsHtml(bpy.types.Operator, OperatorDocGenerator, HtmlOutput, ExportHelper):
    bl_idname = "docs.operators_html"
    bl_label = "Operators Html"
    bl_description = "Operators documentation in html format"

    def generate(self, module):
        string = '<span style="font-size: small;">\n'
        #string += ('<h3>OPERATORS IN bpy.ops.%s</h3>\n' % self.module)
        for opname in dir(module):
            op = getattr(module, opname)
            rnatype = op.get_rna_type()
            string += ('<br /><hr /><span style="font-family: Verdana, sans-serif;">' +
                       '<b>bpy.ops.%s</b>(<i><span style="color: blue;">' % op.idname_py())

            args = []
            multi = getMulti(rnatype)
            for prop in rnatype.properties:
                if skipProp(prop, multi):
                    continue
                args.append(("%s = %s" % (prop.identifier, self.getPropDefault(prop))))
            if len(args) > 2:
                for arg in args:
                    string += ("<br />&emsp;&emsp;&emsp;%s," % arg)
                string = string[:-1]
            elif args:
                for arg in args:
                    string += ("%s, " % arg)
                string = string[:-2]

            string += '</span></i>)</span><br />\n'
            string += ('<br />&nbsp; %s<br />\n' % self.getOpDescription(rnatype))
            if not args:
                continue
            string += '<br /><b>Parameters:</b><br />\n'
            if multi:
                string += ('&emsp;&nbsp; Use <b>import_daz.setSelection</b>' +
                           '(<i><span style="color: blue;">list_of_%s</span></i>)' % multi +
                           ' to specify input %s.<br />\n' % multi)
            string += '<ul>\n'
            for prop in rnatype.properties:
                if skipProp(prop, multi):
                    continue
                string += ('<li>&nbsp; <span style="font-family: &quot;Trebuchet MS&quot;, sans-serif;">' +
                           '%s</span> (<i>%s</i>) - %s</li>\n' % (prop.identifier, self.getPropType(prop), self.getPropDescription(prop)))
            string += '</ul>\n'
        string += '</span>'
        return string

#-------------------------------------------------------------
#   MD operators
#-------------------------------------------------------------

class DOCS_OT_OperatorsMd(bpy.types.Operator, OperatorDocGenerator, MdOutput, ExportHelper):
    bl_idname = "docs.operators_md"
    bl_label = "Operators MD"
    bl_description = "Operators documentation in marcdown format"

    def generate(self, module):
        string = ("## OPERATORS IN bpy.ops.%s ##\n\n" % self.module)
        for opname in dir(module):
            op = getattr(module, opname)
            rnatype = op.get_rna_type()
            string += ("**bpy.ops.%s**(" % op.idname_py())

            args = []
            multi = getMulti(rnatype)
            for prop in rnatype.properties:
                if skipProp(prop, multi):
                    continue
                args.append(("%s = %s" % (prop.identifier, self.getPropDefault(prop))))
            if args:
                string += "*"
                for arg in args:
                    string += "%s, " % arg
                string = "%s*" % string[:-2]
            string += ") \n\n"

            string += ("  %s\n" % self.getOpDescription(rnatype))
            if not args:
                string += self.separator
                continue
            string += "\n** Parameters: **\n\n"
            if multi:
                string += ("  Use import_daz.setSelection(list_of_%s) to specify input %s.\n\n" % (multi, multi))
            for prop in rnatype.properties:
                if skipProp(prop, multi):
                    continue
                string += ("* *%s* (%s) - %s\n" % (prop.identifier, self.getPropType(prop), self.getPropDescription(prop)))
            string += self.separator
        return string

#-------------------------------------------------------------
#   Text operators
#-------------------------------------------------------------

class DOCS_OT_OperatorsText(bpy.types.Operator, OperatorDocGenerator, TextOutput, ExportHelper):
    bl_idname = "docs.operators_text"
    bl_label = "Operators Text"
    bl_description = "Operators documentation in ascii format"

    def generate(self, module):
        string = ("OPERATORS IN bpy.ops.%s\n\n" % self.module)
        string += "=================================================================\n\n"
        for opname in dir(module):
            op = getattr(module, opname)
            rnatype = op.get_rna_type()
            opdef = ("bpy.ops.%s(" % op.idname_py())
            argstart = len(opdef)*" "
            string += opdef

            args = []
            multi = getMulti(rnatype)
            for prop in rnatype.properties:
                if skipProp(prop, multi):
                    continue
                args.append(("%s = %s" % (prop.identifier, self.getPropDefault(prop))))
            if len(args) > 2:
                string += ("%s,\n" % args[0])
                for arg in args[1:]:
                    string += ("%s%s,\n" % (argstart, arg))
                string = string[:-2]
            elif args:
                for arg in args:
                    string += ("%s, " % arg)
                string = string[:-2]
            string += ")\n\n"

            string += ("  %s\n" % self.getOpDescription(rnatype))
            if not args:
                string += self.separator
                continue
            string += "\nParameters:\n"
            if multi:
                string += ("  Use import_daz.setSelection(list_of_%s) to specify input %s.\n\n" % (multi, multi))
            for prop in rnatype.properties:
                if skipProp(prop, multi):
                    continue
                string += ("  %s (%s) - %s\n" % (prop.identifier, self.getPropType(prop), self.getPropDescription(prop)))
            string += self.separator
        return string

#-------------------------------------------------------------
#   Function Docs
#-------------------------------------------------------------

class FunctionDocGenerator:
    module : StringProperty(
        name = "Module",
        description = "Generate function docs for this module",
        default = "import_daz"
    )

    def getFunctions(self, module):
        functions = {}
        n = len(self.module)
        print("MOD", self.module, n)
        for fname in dir(module):
            fcn = getattr(module, fname)
            if (hasattr(fcn, "__module__") and
                hasattr(fcn, "__doc__") and
                fcn.__doc__ and
                fcn.__module__ != self.module and
                fcn.__module__[0:n] == self.module):
                print("FCN", fname)
                functions[fname] = self.getFunction(fcn.__doc__)
        return functions


    def getFunction(self, string):
        lines = [line.strip() for line in string.split("\n") if line]
        rline = aline = 1000
        for n,line in enumerate(lines):
            if line == "Arguments:":
                aline = n
            if line == "Returns:":
                rline = n

        nlines = len(lines)
        dend = min(aline, rline, nlines)
        if aline > rline:
            rend = min(aline, nlines)
            aend = nlines
        else:
            rend = nlines
            aend = min(rline, nlines)

        hdr = lines[0]
        descr = lines[1:dend]
        ret = []
        if rline < 1000:
            ret = lines[rline+1:rend]
        args = []
        if aline < 1000:
            args = lines[aline+1:aend]
        return (hdr, descr, ret, args)


    def execute(self, context):
        from importlib import import_module
        module = import_module(self.module)
        functions = self.getFunctions(module)
        string = self.generate(functions)
        filepath = self.getFilePath()
        with open(filepath, "w") as fp:
            fp.write(string)
        print("%s written" % filepath)
        return {'FINISHED'}

    def invoke(self, context, event):
        context.window_manager.fileselect_add(self)
        return {'RUNNING_MODAL'}

#-------------------------------------------------------------
#   HTML functions
#-------------------------------------------------------------

class DOCS_OT_FunctionsHtml(bpy.types.Operator, FunctionDocGenerator, HtmlOutput, ExportHelper):
    bl_idname = "docs.functions_html"
    bl_label = "Functions Html"
    bl_description = "Functions documentation in html format"

    def generate(self, functions):
        string = '<span style="font-size: small;">\n'
        #string += ('<h3>FUNCTIONS IN bpy.ops.%s</h3>\n' % self.module)
        for fname,fcn in functions.items():
            hdr, descr, ret, arglist = fcn

            fname,args = hdr.split("(")
            string += (
                '<br /><hr /><span style="font-family: Verdana, sans-serif;">' +
                '<b>%s</b>(<i><span style="color: blue;">' % fname +
                args[:-1] +
                '</span></i>)</span><br />\n')

            string += ('<br />&nbsp; %s<br />\n' % "".join(descr))

            if ret:
                string += (
                '<br /><b>Returns:</b><br />\n' +
                '<br />&nbsp; %s<br />\n' % "".join(ret))

            if arglist:
                args = []
                for line in arglist:
                    if line == "":
                        pass
                    elif line[0] == "?":
                        arg,desc = line[1:].split(":", 1)
                        args.append((arg,desc))
                    else:
                        desc += line

                string += '<br /><b>Arguments:</b><br />\n<ul>\n'
                for arg,desc in args:
                    string += ('<li>&nbsp; <span style="font-family: &quot;Trebuchet MS&quot;, sans-serif;">' +
                               '%s</span>: %s</li>\n' % (arg, desc))
                string += '</ul>\n'
        string += '</span>'
        return string

#-------------------------------------------------------------
#   Text functions
#-------------------------------------------------------------

class DOCS_OT_FunctionsText(bpy.types.Operator, FunctionDocGenerator, TextOutput, ExportHelper):
    bl_idname = "docs.functions_text"
    bl_label = "Functions Text"
    bl_description = "Functions documentation in ascii format"

    def generate(self, functions):
        string = ("FUNCTIONS IN %s\n\n" % self.module)
        string += "=================================================================\n\n"
        for fname,fcn in functions.items():
            hdr, descr, ret, args = fcn
            string += hdr + "\n"
            for dline in descr:
                string += "\n" + dline
            if ret:
                string += "\n\nReturns:"
                for line in ret:
                    string += "\n  %s" % line
            if args:
                string += "\n\nArguments:"
                for line in args:
                    if line == "":
                        pass
                    elif line[0] == "?":
                        string += "\n\n  %s" % line[1:]
                    else:
                        string += "\n    %s" % line
                string += "\n"
            string += self.separator
        return string + "\n"

#-------------------------------------------------------------
#   MD functions
#-------------------------------------------------------------

class DOCS_OT_FunctionsMd(bpy.types.Operator, FunctionDocGenerator, MdOutput, ExportHelper):
    bl_idname = "docs.functions_md"
    bl_label = "Functions MD"
    bl_description = "Functions documentation in markdown format"

    def generate(self, functions):
        string = ("## FUNCTIONS IN %s ##\n\n" % self.module)
        string += self.separator
        for fname,fcn in functions.items():
            hdr, descr, ret, args = fcn
            string += self.formatHeader(hdr)
            for dline in descr:
                string += "\n" + dline
            if ret:
                string += "\n\n** Returns: **"
                for line in ret:
                    string += "\n  %s" % line
            if args:
                string += "\n\n** Arguments: **"
                for line in args:
                    if line == "":
                        pass
                    elif line[0] == "?":
                        string += "\n\n  %s" % line[1:]
                    else:
                        string += "\n    %s" % line
                string += "\n"
            string += self.separator
        return string + "\n"

#----------------------------------------------------------
#   Utils
#----------------------------------------------------------

def getMulti(rnatype):
    propids = [prop.identifier for prop in rnatype.properties]
    if "files" in propids:
        return "files"
    if "selection" in propids:
        return "selection"
    return False


def skipProp(prop, multi):
    if prop.type in ["POINTER", "COLLECTION"]:
        return True
    if prop.identifier in ["filter_glob", "filter"]:
        return True
    if prop.identifier in ["filepath", "directory"]:
        return multi
    return False

#----------------------------------------------------------
#   Initialize
#----------------------------------------------------------

classes = [
    DOCS_OT_FunctionsText,
    DOCS_OT_FunctionsHtml,
    DOCS_OT_FunctionsMd,
    DOCS_OT_OperatorsText,
    DOCS_OT_OperatorsHtml,
    DOCS_OT_OperatorsMd,
]

def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
