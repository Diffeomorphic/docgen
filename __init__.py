# SPDX-FileCopyrightText: 2020-2024, Thomas Larsson
#
# SPDX-License-Identifier: GPL-2.0-or-later

bl_info = {
    'name': 'Documentation Generator',
    'author': 'Thomas Larsson',
    'version': (1,0),
    "blender": (4,2,0),
    'location': "UI panel",
    'description': "Generate documentation for Blender add-ons",
    'warning': '',
    'wiki_url': "/diffeomorphic.blogspot.se/p/",
    'category': 'Documentation'}

if "bpy" in locals():
    print("Reloading DocGen v %d.%d" % bl_info["version"])
    import imp
    imp.reload(generate)
    imp.reload(buttons)
else:
    print("Loading DocGen v %d.%d" % bl_info["version"])
    from . import generate
    from . import buttons

import bpy

class DOCS_PT_Main(bpy.types.Panel):
    bl_category = "DocGen"
    bl_label = "DocGen"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        self.layout.operator("docs.functions_text")
        self.layout.operator("docs.functions_html")
        self.layout.operator("docs.functions_md")
        self.layout.operator("docs.operators_text")
        self.layout.operator("docs.operators_html")
        self.layout.operator("docs.operators_md")
        self.layout.operator("docs.buttons_md")

#----------------------------------------------------------
#   Initialize
#----------------------------------------------------------

classes = [
    DOCS_PT_Main,
]

def register():
    generate.register()
    buttons.register()

    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    generate.unregister()
    buttons.unregister()

    for cls in classes:
        bpy.utils.unregister_class(cls)


if __name__ == "__main__":
    register()

print("Gen Docs loaded")

