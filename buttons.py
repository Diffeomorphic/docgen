# SPDX-FileCopyrightText: 2020-2024, Thomas Larsson
#
# SPDX-License-Identifier: GPL-2.0-or-later

import os
import bpy
from bpy.props import *
from bpy_extras.io_utils import ExportHelper

#-------------------------------------------------------------
#  Features
#-------------------------------------------------------------

Features = [
    ("Main Tools", "root"),
    ("Rigging Tools", "rig_tools"),
    ("Simple IK", "simple_ik"),
    ("MHX", "rig_mhx"),
    ("Rigify", "rig_rigify"),
    ("Pose Tools", "pose_tools"),
    ("Object Tools", "object_tools"),
    ("Material Tools", "material_tools"),
    ("Mesh Tools", "mesh_tools"),
    ("Morph Tools", "morph_tools"),
    ("Hair Tools", "hair_tools"),
    ("Visibility Tools", "visibility_tools"),
    ("HD Tools", "hd_tools"),
    ("Simulation Tools", "simulation_tools"),
    ("Export Tools", "export_tools"),
    ("Shell Editor", "shell_edit")
]

#-------------------------------------------------------------
#   MD buttons
#-------------------------------------------------------------

class ButtonInfo:
    def __init__(self, cls):
        self.name = ""
        self.label = ""
        self.description = "Undocumented tool"

    def __repr__(self):
        return ("* **%s**\n" % self.label +
                "    * *%s*\n" % self.description +
                "    * %s\n\n" % self.name
                )


class DOCS_OT_ButtonsMd(bpy.types.Operator):
    bl_idname = "docs.buttons_md"
    bl_label = "Buttons MD"
    bl_description = "Buttons documentation in marcdown format"

    def execute(self, context):
        def getInfos(folder, features, infos):
            for file in os.listdir(folder):
                path = os.path.join(folder, file)
                if (file.startswith(".") or
                    file in ["data", "to_daz_studio"]):
                    pass
                elif os.path.isdir(path):
                    ninfos = []
                    features[file] = ninfos
                    getInfos(path, features, ninfos)
                    getDescriptions(ninfos)
                elif os.path.splitext(path)[-1] == ".py":
                    with open(path, "r") as fp:
                        getFileInfos(fp, infos)

        def getFileInfos(fp, infos):
            info = None
            for line in fp:
                line = line.lstrip().rstrip()
                if line.startswith("class DAZ_OT_"):
                    cls = line.split("(")[0]
                    info = ButtonInfo(cls)
                    infos.append(info)
                elif info:
                    words = line.split(" = ")
                    if words[0] == "bl_idname":
                        info.name = words[1].strip('"')
                    elif words[0] == "bl_label":
                        info.label = words[1].strip('"')
                    elif words[0] == "bl_description":
                        info.description = words[1].strip('"')

        def getDescriptions(infos):
            for info in infos:
                if info.name[0:4] == "daz.":
                    opname = info.name[4:]
                op = getattr(bpy.ops.daz, opname)
                try:
                    rnatype = op.get_rna_type()
                    info.description = rnatype.description.replace("\n", " ")
                except KeyError:
                    print("Missing operator:", info.name)

        infos = []
        features = {"root" : infos}
        path = "D:/home/hg/import_daz"
        getInfos(path, features, infos)
        getDescriptions(infos)

        string = "## List of tools ordered by feature ##\n"
        for label,key in Features:
            infos = features.get(key)
            if infos is None:
                print("MISS", key)
                continue
            string += "\n### %s ###\n\n" % label
            for info in infos:
                string += info.__repr__()

        filepath = "D:/home/output.md"
        with open(filepath, "w") as fp:
            fp.write(string)
        print("%s written" % filepath)
        return {'FINISHED'}

#----------------------------------------------------------
#   Initialize
#----------------------------------------------------------

classes = [
    DOCS_OT_ButtonsMd,
]

def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)

